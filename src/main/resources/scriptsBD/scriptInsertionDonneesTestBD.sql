insert into hackathon(name, topic, description)
values ('HackAct', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.');
insert into hackathon(name, topic, description)
values ('HackInnov', 'Services publics et Innovation',
        'Ce hackathon vise à réunir les citoyens d’une communauté pour développer des solutions afin d’améliorer les services publics offerts aux citoyens');
insert into hackathon(name, topic, description)
values ('HackConsul', 'Réinventer un métier',
        'Ce hackathon vise à réunir des étudiants afin de réinventer le métier de Consultant');

insert into member(firstname, lastname)
values ('Mathieu', 'Grisse'),
       ('Sara', 'Brucker'),
       ('Florence', 'Markus'),
       ('Thomas', 'Pasque'),
       ('Marc', 'Tombour'),
       ('Cindy', 'Akbou'),
       ('Mathilde', 'Antas'),
       ('Teddy', 'Martin'),
       ('Julien', 'Lourau'),
       ('Charlie', 'Haden');

insert into role(name)
values ('jury'),
        ('inscrit');

insert into participation(hackathonid,memberid,roleid)
values (1,1,1),
       (1,2,1),
       (1,3,1),
       (1,4,1),
       (2,5,1),
       (2,6,1),
       (3,7,1),
       (3,8,1),
       (1,9,2),
       (1,10,2);
