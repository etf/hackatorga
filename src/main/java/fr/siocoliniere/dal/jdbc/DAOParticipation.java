package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.bo.Participation;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class DAOParticipation extends DAO<Participation> {

    private static DAOParticipation instanceDAOParticipation;
    private Member obj;


    public static synchronized DAOParticipation getInstance() {
        if (instanceDAOParticipation == null) {
            instanceDAOParticipation = new DAOParticipation();
        }
        return instanceDAOParticipation;
    }


    private static final String sqlInsertParticipation = "insert into participation(hackathonid, memberid, roleid) values (?,?,?)";

    @Override
    public List<Participation> getAll() throws DALException {
        return null;
    }

    @Override
    public Participation getOneById(int id) throws DALException {
        return null;
    }

    @Override
    public void save(Participation obj) throws DALException {

    }

    @Override
    public long newMembre(Member obj) throws DALException {

        return 0;
    }

    @Override
    public void update(Participation obj) throws DALException {

    }

    public void newParticipation(Participation obj) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertParticipation);
            rqt.setInt(1, obj.getHackathonid());
            rqt.setInt(2, obj.getMemberid());
            rqt.setInt(3, obj.getRole());
            rqt.executeUpdate();
            rqt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
