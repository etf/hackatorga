package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.bo.Participation;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOMember
 * Implémente la classe abstraite DAO
 * BD : Table Member
 */
public class DAOMember extends DAO<Member> {

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static DAOMember instanceDAOMember;
    private Member obj;

    /**
     * Pattern Singleton
     *
     * @return DAOMember
     */
    public static synchronized DAOMember getInstance() {
        if (instanceDAOMember == null) {
            instanceDAOMember = new DAOMember();
        }
        return instanceDAOMember;
    }

    /**
     * REQUETES
     */
    private static final String sqlSelectOneById = "select member.id,member.firstname, member.lastname from member where member.id = ?";
    private static final String sqlSelectAllParticipationJuryByIdHackathon = "select member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.hackathonid = ? and participation.roleid = 1";
    private static final String sqlSelectAllJuryMember = "select distinct member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.roleid = 1";
    private static final String sqlSelectAllMember = "select distinct member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid ";


    private static final String sqlInsertJury = "insert into member(firstname, lastname) values (?,?)";
    private static final String sqlInsertParticipation = "insert into participation(hackathonid, memberid, roleid) values (?,?,?)";

    /**
     * Permet de récupérer l'ensemble des membres stockés dans la table membre
     *
     * @return
     * @throws DALException
     */
    @Override
    public List<Member> getAll() throws DALException {
        // TO IMPLEMENT
        return null;
    }

    /**
     * Permet de récupérer l'ensemble des membres ayant participé à un hackathon avec le rôle "jury" (roleid = 1)
     *
     * @return liste de membres
     * @throws DALException
     */
    public List<Member> getAllJuryMember() throws DALException {

        List<Member> memberList = new ArrayList<>();
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAllJuryMember);

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"));
                memberList.add(member);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(), e);
        }
        return memberList;
    }
    public List<Member> getAllMember() throws DALException {

        List<Member> memberListe = new ArrayList<>();
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAllMember);

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"));
                memberListe.add(member);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(), e);
        }
        return memberListe;
    }

    /**
     * Permet de récupérer l'ensemble des membres ayant participé au hackathon dont l'id est passé en paramètre, avec le rôle "jury" (roleid = 1)
     *
     * @param hackid
     * @return liste de membres
     * @throws DALException
     */
    public List<Member> getAllJuryByIdHackathon(int hackid) throws DALException {

        List<Member> juries = new ArrayList<>();

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllParticipationJuryByIdHackathon);
            rqt.setInt(1, hackid);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                juries.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname")));
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        return juries;
    }

    /**
     * Permet de récupérer le membre dont l'identifiant est passé en paramètre
     *
     * @param id
     * @return un membre
     * @throws DALException
     */
    @Override
    public Member getOneById(int id) throws DALException {
        Member member = null;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"));
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        return member;
    }

    @Override
    public void save(Member obj) throws DALException {

    }


    public long newMembre(Member obj) throws DALException {
        try {
            long id;
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertJury,PreparedStatement.RETURN_GENERATED_KEYS);
            rqt.setString(1, obj.getFirstname());
            rqt.setString(2, obj.getlastname());
            rqt.executeUpdate();
            ResultSet rs = rqt.getGeneratedKeys();
            if (!rs.next()) {
                return 0;
            } else
                 id = rs.getLong(1);
            rqt.close();
            return id;
        } catch (SQLException e) {
            return 0;
        }
    }

    public void newParticipation(Participation obj) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertParticipation);
            rqt.setInt(1, obj.getHackathonid());
            rqt.setInt(2, obj.getMemberid());
            rqt.setInt(3, obj.getRole());
            rqt.executeUpdate();
            rqt.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public void update(Member obj) throws DALException {
        // TO IMPLEMENT
    }
}