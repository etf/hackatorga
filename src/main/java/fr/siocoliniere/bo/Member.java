package fr.siocoliniere.bo;

/**
 * Classe métier Member
 * Permet l'instanciation d'un membre
 */
public class Member {
    private int id;
    private String lastname;
    private String firstname;

    /**
     * Constructeur
     * @param lastname nom du membre
     * @param firstname prenom du membre
     */
    public Member(String lastname, String firstname) {
        this.lastname = lastname;
        this.firstname = firstname;
    }
    public Member(int id, String lastname, String firstname) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getlastname() {return lastname;}
    public String getFirstname() {return firstname;}

    @Override
    public String toString() {
        return  lastname + ' ' + firstname ;
    }
}
