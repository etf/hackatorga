package fr.siocoliniere.bo;

public class Participation {
    private int id;
    private int hackathonid;
    private int memberid;
    private int role;


    public Participation(int id, int hackathonid, int memberid, int role) {
        this.id = id;
        this.hackathonid = hackathonid;
        this.memberid = memberid;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHackathonid() {
        return hackathonid;
    }

    public void setHackathonid(int hackathonid) {
        this.hackathonid = hackathonid;
    }

    public int getMemberid() {
        return memberid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
