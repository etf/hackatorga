package fr.siocoliniere.bo;

public class Lieu {
    private int id;

    private String nom;

    public Lieu(int id, String nom ) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }
    public String  getNom() {
        return nom;
    }

    public String toString() {
        return  nom ;
    }



    }
