package fr.siocoliniere.bo;

public class Competence {
    private int id;
    private String name;

    public Competence(int id, String name ) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public String  getName() {
        return name;
    }

    @Override
    public String toString() {
        return  id + ' ' + name ;
    }
}

