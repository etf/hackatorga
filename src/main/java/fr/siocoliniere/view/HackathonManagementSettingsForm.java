package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementSettingsForm extends JDialog {

    private Hackathon hackathonManaged;

    /**
     * Constructeur
     * @param owner
     */
    public HackathonManagementSettingsForm(Window owner) {
        super(owner);

        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));

        setTitle("Hackat'Orga : Settings ");

        initComponents();
    }

    /**
     * Permet d'initialiser la vue pour un hacakthon choisi
     * @param hackathonManaged
     */
    public void initHackathonManaged(Hackathon hackathonManaged) {
        this.hackathonManaged = hackathonManaged;

        if (this.hackathonManaged != null) {

            //settings globales : initialisation à partir des données du hackathon
            nameTextField.setText(hackathonManaged.getName());
            topicTextField.setText(hackathonManaged.getTopic());
            descriptionTextArea.setText(hackathonManaged.getDescription());

        }else {
            nameTextField.setText("");
            topicTextField.setText("");
            descriptionTextArea.setText("");

        }
    }

    /**
     * Permet de sauvegarder les settings du hackathon
     * Appelée : lors du clic sur le bouton Save
     */
    private void saveButtonActionPerformed(ActionEvent e) {
        HackathonController.getInstance().saveHackathonStandalone(hackathonManaged,nameTextField.getText(),topicTextField.getText(),descriptionTextArea.getText());
        this.dispose();
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }


    /**
     * JFormDesigner - DO NOT MODIFY
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        nameLabel = new JLabel();
        nameTextField = new JTextField();
        topicLabel = new JLabel();
        topicTextField = new JTextField();
        descriptionLabel = new JLabel();
        descriptionScrollPane = new JScrollPane();
        descriptionTextArea = new JTextArea();
        lieu = new JLabel();
        scrollPane1 = new JScrollPane();
        lieulist = new JList();
        dateDebut = new JLabel();
        DateDebutTextField = new JTextField();
        dateFin = new JLabel();
        DateFinTextField = new JTextField();
        Competence = new JLabel();
        competenceTextField = new JTextField();
        buttonBar = new JPanel();
        saveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 97, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- nameLabel ----
                nameLabel.setText("Name");
                contentPanel.add(nameLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(nameTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- topicLabel ----
                topicLabel.setText("Topic");
                contentPanel.add(topicLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(topicTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 15, 0), 0, 0));

                //---- descriptionLabel ----
                descriptionLabel.setText("Description");
                contentPanel.add(descriptionLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 10, 5, 5), 0, 0));

                //======== descriptionScrollPane ========
                {
                    descriptionScrollPane.setViewportView(descriptionTextArea);
                }
                contentPanel.add(descriptionScrollPane, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- lieu ----
                lieu.setText("Lieu");
                contentPanel.add(lieu, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(lieulist);
                }
                contentPanel.add(scrollPane1, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- dateDebut ----
                dateDebut.setText("Date D\u00e9but");
                contentPanel.add(dateDebut, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(DateDebutTextField, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- dateFin ----
                dateFin.setText("Date Fin");
                contentPanel.add(dateFin, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(DateFinTextField, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- Competence ----
                Competence.setText("Comp\u00e9tence");
                contentPanel.add(Competence, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                contentPanel.add(competenceTextField, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- saveButton ----
                saveButton.setText("Save");
                saveButton.addActionListener(e -> saveButtonActionPerformed(e));
                buttonBar.add(saveButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel topicLabel;
    private JTextField topicTextField;
    private JLabel descriptionLabel;
    private JScrollPane descriptionScrollPane;
    private JTextArea descriptionTextArea;
    private JLabel lieu;
    private JScrollPane scrollPane1;
    private JList lieulist;
    private JLabel dateDebut;
    private JTextField DateDebutTextField;
    private JLabel dateFin;
    private JTextField DateFinTextField;
    private JLabel Competence;
    private JTextField competenceTextField;
    private JPanel buttonBar;
    private JButton saveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
