package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

public class HackathonManagementJuryForm extends JDialog {

    private Hackathon hackathonManaged;
    private HackathonJuryForm viewHackathonJury;
    public HackathonManagementJuryForm(Window owner) {
        super(owner);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(700, 700));

        setTitle("Hackat'Orga : Jury ");

        viewHackathonJury = new HackathonJuryForm(this);


        initComponents();

        //initialisation de la JList des jury potentiels
        initPotentialJuryList(HackathonController.getInstance().getPotentialJury());
        initMemberListe(HackathonController.getInstance().getAllMember());
    }

    /**
     * Permet d'initialiser la vue pour un hackathon choisi
     * @param hackathonManaged
     */
    public void initHackathonManaged(Hackathon hackathonManaged){
        this.hackathonManaged = hackathonManaged;
        if (this.hackathonManaged != null) {
            // initialisation de la JList des jury existants
            initJuryList(hackathonManaged.getJuryMembers());
        }
        else{
            initJuryList(new ArrayList<>());
        }
    }

    /**
     * Permet de valoriser la Jlist des membres actuels du hackathon
     * @param juries liste des membres du jury
     */
    private void initJuryList(java.util.List<Member> juries) {
        //!!! Info technique : un objet de type JList fonctionne avec un objet de type DefaultListModel qui va contenir les données

        //création du DefaultListModel à partir de la liste d'objets de type Member
        DefaultListModel<Member> modelJuryList = new DefaultListModel<>();
        for (Member member : juries) {
            modelJuryList.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.juryJList.setModel(modelJuryList);

    }

    /**
     * Permet de valoriser la liste des jury potentiels
     * @param memberList liste de jury
     */
    private void initPotentialJuryList(List<Member> memberList) {
        //!!! Info technique : un objet de type JList fonctionne avec un objet de type DefaultListModel qui va contenir les données

        //création du DefaultListModel à partir de la liste d'objets de type Jury
        DefaultListModel<Member> modelPotentialJuryList = new DefaultListModel<>();
        for (Member member : memberList) {
            modelPotentialJuryList.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.potentialJuryJList.setModel(modelPotentialJuryList);

    }
    private void initMemberListe(List<Member> memberListe) {
        //!!! Info technique : un objet de type JList fonctionne avec un objet de type DefaultListModel qui va contenir les données

        //création du DefaultListModel à partir de la liste d'objets de type Jury
        DefaultListModel<Member> modelMemberListe = new DefaultListModel<>();
        for (Member member : memberListe) {
            modelMemberListe.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.memberList.setModel(modelMemberListe);

    }


    /**
     * METHODES RATTACHEES A LA GESTION AUX EVENEMENTS
     */

    /**
     * Permet de retirer le membre sélectionné de la liste du jury
     * Appelée : lors du clic sur le bouton Remove
     */
    private void removeJurryToMemberButtonActionPerformed(ActionEvent e) {
        //récupération du jury sélectionné
        Member jurySelected = (Member) juryJList.getSelectedValue();

        DefaultListModel<Member> modelJuryListe = (DefaultListModel<Member>) juryJList.getModel();
        modelJuryListe.removeElement(jurySelected);

        DefaultListModel<Member> modelMemberListe = (DefaultListModel<Member>) memberList.getModel();
        modelMemberListe.addElement(jurySelected);
    }
    private void removeButtonActionPerformed(ActionEvent e) {
        // TODO add your code here
        //récupération du jury sélectionné
        Member jurySelected = (Member) juryJList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant le jury du hackathon
        DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) juryJList.getModel();
        modelJuryList.removeElement(jurySelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les membres de jury potentiels
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialJuryJList.getModel();
        modelMemberList.addElement(jurySelected);
    }
    private void addButtonActionPerformed(ActionEvent e) {
        //TODO : to implement
        //récupération du jury sélectionné
        Member jurySelected = (Member) potentialJuryJList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant le jury du hackathon
        DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) potentialJuryJList.getModel();
        modelJuryList.removeElement(jurySelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les membres de jury potentiels
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) juryJList.getModel();
        modelMemberList.addElement(jurySelected);


        System.out.println("To implement");    }

    /**
     * Permet d'ajouter le jury sélectionné de la liste du jury
     * Appelée : lors du clic sur le bouton Add
     */
    private void addMemberToJuryButtonActionPerformed(ActionEvent e) {

        //récupération du jury sélectionné
        Member jurySelected = (Member) memberList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant le jury du hackathon
        DefaultListModel<Member> modelJuryListe = (DefaultListModel<Member>) memberList.getModel();
        modelJuryListe.removeElement(jurySelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les membres de jury potentiels
        DefaultListModel<Member> modelMemberListe = (DefaultListModel<Member>) juryJList.getModel();
        modelMemberListe.addElement(jurySelected);


        System.out.println("To implement");    }

    /**
     * Permet de sauvegarder la liste de jury du hackathon
     * Appelée : lors du clic sur le bouton Save
     */
    private void SaveButtonActionPerformed(ActionEvent e) {

        // Construction de la liste de jury à sauvegarder
        List<Member> juries = new ArrayList<>();

        for (int i =0; i< juryJList.getModel().getSize(); i++){
            Member jury = (Member) juryJList.getModel().getElementAt(i);
            juries.add(jury);
        }

        HackathonController.getInstance().saveParticipationJuryByIdHackathon(hackathonManaged,juries);
        this.dispose();
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void nouveauJury(ActionEvent e) {

        viewHackathonJury.initHackathonManaged(hackathonManaged);
        viewHackathonJury.setVisible(true);
    }

    private void thisWindowActivated(WindowEvent e) {
        initJuryList(MemberController.getInstance().getAllByRoleJury());

    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        existingLabel = new JLabel();
        potentialLabel = new JLabel();
        memberLabel = new JLabel();
        juryMemberScrollPane = new JScrollPane();
        juryJList = new JList();
        ARButtonPanel = new JPanel();
        addButton = new JButton();
        removeButton = new JButton();
        juryPotentiaScrollPane = new JScrollPane();
        potentialJuryJList = new JList();
        ARButtonPanel2 = new JPanel();
        addToJurry = new JButton();
        removeToMember = new JButton();
        memberScrollPane = new JScrollPane();
        memberList = new JList();
        buttonBar = new JPanel();
        SaveButton2 = new JButton();
        SaveButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowActivated(WindowEvent e) {
                thisWindowActivated(e);
            }
        });
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 87, 88, 94, 0, 44, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};

                //---- existingLabel ----
                existingLabel.setText("Existing");
                contentPanel.add(existingLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 5), 0, 0));

                //---- potentialLabel ----
                potentialLabel.setText("Potential");
                contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 5), 0, 0));

                //---- memberLabel ----
                memberLabel.setText("Member");
                contentPanel.add(memberLabel, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 0), 0, 0));

                //======== juryMemberScrollPane ========
                {
                    juryMemberScrollPane.setViewportView(juryJList);
                }
                contentPanel.add(juryMemberScrollPane, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(5, 5, 6, 10), 0, 0));

                //======== ARButtonPanel ========
                {
                    ARButtonPanel.setLayout(new GridBagLayout());
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
                    ((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                    //---- addButton ----
                    addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
                    addButton.setText("Add");
                    addButton.addActionListener(e -> addButtonActionPerformed(e));
                    ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));

                    //---- removeButton ----
                    removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
                    removeButton.setText("Remove");
                    removeButton.addActionListener(e -> removeButtonActionPerformed(e));
                    ARButtonPanel.add(removeButton, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));
                }
                contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 5), 0, 0));

                //======== juryPotentiaScrollPane ========
                {
                    juryPotentiaScrollPane.setViewportView(potentialJuryJList);
                }
                contentPanel.add(juryPotentiaScrollPane, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 5), 0, 0));

                //======== ARButtonPanel2 ========
                {
                    ARButtonPanel2.setLayout(new GridBagLayout());
                    ((GridBagLayout)ARButtonPanel2.getLayout()).columnWidths = new int[] {0, 0};
                    ((GridBagLayout)ARButtonPanel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)ARButtonPanel2.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
                    ((GridBagLayout)ARButtonPanel2.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4};

                    //---- addToJurry ----
                    addToJurry.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
                    addToJurry.setText("Add");
                    addToJurry.addActionListener(e -> addMemberToJuryButtonActionPerformed(e));
                    ARButtonPanel2.add(addToJurry, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));

                    //---- removeToMember ----
                    removeToMember.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
                    removeToMember.setText("Remove");
                    removeToMember.addActionListener(e -> removeJurryToMemberButtonActionPerformed(e));
                    ARButtonPanel2.add(removeToMember, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 0), 0, 0));
                }
                contentPanel.add(ARButtonPanel2, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 5), 0, 0));

                //======== memberScrollPane ========
                {
                    memberScrollPane.setViewportView(memberList);
                }
                contentPanel.add(memberScrollPane, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 6, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- SaveButton2 ----
                SaveButton2.setText("nouveau jury");
                SaveButton2.addActionListener(e -> nouveauJury(e));
                buttonBar.add(SaveButton2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- SaveButton ----
                SaveButton.setText("Save");
                SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
                buttonBar.add(SaveButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel existingLabel;
    private JLabel potentialLabel;
    private JLabel memberLabel;
    private JScrollPane juryMemberScrollPane;
    private JList juryJList;
    private JPanel ARButtonPanel;
    private JButton addButton;
    private JButton removeButton;
    private JScrollPane juryPotentiaScrollPane;
    private JList potentialJuryJList;
    private JPanel ARButtonPanel2;
    private JButton addToJurry;
    private JButton removeToMember;
    private JScrollPane memberScrollPane;
    private JList memberList;
    private JPanel buttonBar;
    private JButton SaveButton2;
    private JButton SaveButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
