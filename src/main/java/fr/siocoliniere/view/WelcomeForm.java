package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import net.miginfocom.swing.*;

public class WelcomeForm extends JFrame {

    private HackathonManagementForm viewHackathonManagement;

    /**
     * Constructeur
     */
    public WelcomeForm() {

        setTitle("Hackat'Orga");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(600, 200));

        initComponents();
        errorLabel.setVisible(false);

        initComboBoxHackathons();

        this.viewHackathonManagement = new HackathonManagementForm();
        this.viewHackathonManagement.setVisible(false);

    }


    /**
     * Permet d'initialiser la liste déroulante avec les hackathons existants
     */
    private void initComboBoxHackathons() {
        //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

        // récupération des hackathons
        List<Hackathon> hackathons = HackathonController.getInstance().getAll();

        // création d'un model à partir de la liste d'objets hackathons
        if (hackathons != null) {
            DefaultComboBoxModel<Hackathon> modelCombo = new DefaultComboBoxModel<>();
            for (Hackathon hacka : hackathons) {
                modelCombo.addElement(hacka);
            }
            //rattachement du DefaultComboBoxModel au composant graphique ComboBox
            hackathonsComboBox.setModel(modelCombo);

        } else {
            //gestion d'une erreur de récupération des données
            this.errorLabel.setText("Problème de chargement de données");
            this.errorLabel.setVisible(true);
            this.manageButton.setEnabled(false);
        }
    }


    /**
     * METHODES RATTACHEES A LA GESTION AUX EVENEMENTS
     */

    /**
     * Permet de quitter l'application
     * Appelée : lors du clic sur le bouton Close
     */
    private void closeButtonActionPerformed(ActionEvent e) {

        ConfirmForm confirmView = new ConfirmForm(this,"Confirmation", "Voulez-vous quitter l'application ?");
        confirmView.pack();
        confirmView.setVisible(true);
        if (confirmView.isAnswer()) {
            // close confirmed
            System.exit(0);
        }
    }

    /**
     * Permet de gérer le hackathon sélectionné
     * Appelée : lors du clic sur le bouton Manage
     */
    private void manageButtonActionPerformed(ActionEvent e) {

        //récupération du hackathon sélectionné
        Hackathon hackathonSelected = (Hackathon) hackathonsComboBox.getSelectedItem();

        //valorisation du hackathon sélectionné sur la vue permettant de gérer un hackathon et affichage de la vue
        this.viewHackathonManagement.setHackathon(hackathonSelected);
        this.viewHackathonManagement.setVisible(true);

    }

    /**
     * Permet de gérer un nouvel hackathon
     * Appelée : lors du clic sur le bouton New Hackathon
     */
    private void newHackaButtonActionPerformed(ActionEvent e) {
        //valorisation du hackathon sélectionné sur la vue permettant de gérer un hackathon et affichage de la vue
        this.viewHackathonManagement.setHackathon(null);
        this.viewHackathonManagement.setVisible(true);
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */
    
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        errorLabel = new JLabel();
        hackathonsComboBox = new JComboBox();
        manageButton = new JButton();
        newHackathonButton = new JButton();
        buttonBar = new JPanel();
        closeButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new MigLayout(
                    "insets dialog,hidemode 3,alignx center",
                    // columns
                    "[fill]" +
                    "[fill]",
                    // rows
                    "[grow]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[]" +
                    "[grow]"));

                //---- errorLabel ----
                errorLabel.setText("Error Message");
                contentPanel.add(errorLabel, "cell 0 1");
                contentPanel.add(hackathonsComboBox, "cell 0 2");

                //---- manageButton ----
                manageButton.setText("Manage");
                manageButton.addActionListener(e -> manageButtonActionPerformed(e));
                contentPanel.add(manageButton, "cell 1 2");

                //---- newHackathonButton ----
                newHackathonButton.setText("New hackathon");
                newHackathonButton.addActionListener(e -> newHackaButtonActionPerformed(e));
                contentPanel.add(newHackathonButton, "cell 0 4");
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setLayout(new MigLayout(
                    "insets dialog,alignx right",
                    // columns
                    "[button,fill]",
                    // rows
                    null));

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, "cell 0 0");
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel errorLabel;
    private JComboBox hackathonsComboBox;
    private JButton manageButton;
    private JButton newHackathonButton;
    private JPanel buttonBar;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


}
