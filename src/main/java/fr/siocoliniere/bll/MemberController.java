package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.bo.Participation;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOMember;
import fr.siocoliniere.dal.jdbc.DAOParticipation;

import java.util.List;

public class MemberController {

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static MemberController instanceCtrl;

    /**
     * Pattern Singleton
     * @return MemberController
     */
    public static synchronized MemberController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new MemberController();
        }
        return instanceCtrl;
    }

    /**
     * CONSTRUCTEUR
     */
    private MemberController() {
    }
    /**
     * Permet de récupérer l'ensemble des membres ayant été jury
     * @return la liste des membres
     */
    public List<Member> getAllByRoleJury(){
        List<Member> juries = null;
        try {
            juries = DAOMember.getInstance().getAllJuryMember();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return juries;
    }
    public void saveJuryStandalone(String nom, String prenom, int idHackathon) {
            // Nouveau Jury
        Member juryManaged = new Member(nom, prenom);

            //persistance : Insert
        try {
            long res = DAOMember.getInstance().newMembre(juryManaged);
            juryManaged.setId((int) res);
            Participation participation = new Participation(1, idHackathon, (int) res, 1 );
            DAOMember.getInstance().newParticipation(participation);
            getAllByRoleJury().add(juryManaged);

        } catch (DALException e) {
            e.printStackTrace();
            }
        }
    }



