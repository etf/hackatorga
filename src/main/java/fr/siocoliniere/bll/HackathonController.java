package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.bo.Competence;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;

import java.util.List;

public class HackathonController {

    private List<Hackathon> hackathons;

    /**
     * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
     */
    private static HackathonController instanceCtrl;

    /**
     * Pattern Singleton
     * @return HackathonController
     */
    public static synchronized HackathonController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new HackathonController();
        }
        return instanceCtrl;
    }

    /**
     * Constructeur
     * Chargement de la liste des hackathons
     * En private : pattern Singleton
     */
    private HackathonController() {

        try {
            this.hackathons = DAOHackathon.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'ensemble des hackathons
     * @return la liste des hackathons
     */
    public List<Hackathon> getAll(){
        return hackathons;
    }

    /**
     * work in progress
     */
    public List<Member> getPotentialJury() {
        return MemberController.getInstance().getAllByRoleJury();
    }
    public List<Member> getAllMember () {
        return MemberController.getInstance().getAllMember();
    }
    /**
     * Permet de sauvegarder les settings d'un hackathon
     * @param hackathonManaged
     * @param name nom du hacakthon
     * @param topic topic du hackathon
     * @param description description du hackathon
     */
    public void saveHackathonStandalone(Hackathon hackathonManaged, String name, String topic, String description) {

        if (hackathonManaged != null) {
            // MAJ hackathon existant
            hackathonManaged.setName(name);
            hackathonManaged.setTopic(topic);
            hackathonManaged.setDescription(description);
           // hackathonManaged.setIdCompetence(idcompetence);
            //persistance : Update
            try {
                DAOHackathon.getInstance().updateSettings(hackathonManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        } else {
            // Nouvel hackathon
            hackathonManaged = new Hackathon(name, topic, description);
            //persistance : Insert
            try {
                DAOHackathon.getInstance().saveHackathon(hackathonManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Permet de persister l'ensemble des membres jouant le rôle de jury du hackathon
     * @param hackathonManaged : hackathon
     * @param juries : liste de membres
     */
    public void saveParticipationJuryByIdHackathon(Hackathon hackathonManaged, List<Member> juries) {

        // maj objet
        hackathonManaged.clearJuryMember();
        hackathonManaged.setJuryMembers(juries);

        //persistance
        try {
            //suppression des participations de membres en tant que jurys stockées en BDD
            DAOHackathon.getInstance().deleteAllParticipationJuryOfHackathon(hackathonManaged.getId());

            // insertion des nouveaux jurys
            for (Member member :juries) {
                DAOHackathon.getInstance().saveParticipationRoleJury(hackathonManaged.getId(),member);
            }

        } catch (DALException e) {
            e.printStackTrace();
        }
    }
}
